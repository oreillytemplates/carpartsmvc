package com.oreillyauto.domain.carparts;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QStoreLocation is a Querydsl query type for StoreLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QStoreLocation extends EntityPathBase<StoreLocation> {

    private static final long serialVersionUID = -621374396L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QStoreLocation storeLocation = new QStoreLocation("storeLocation");

    public final QCarpart carpart;

    public final StringPath storeDetails = createString("storeDetails");

    public final NumberPath<Integer> storeId = createNumber("storeId", Integer.class);

    public final NumberPath<Integer> storeNumber = createNumber("storeNumber", Integer.class);

    public QStoreLocation(String variable) {
        this(StoreLocation.class, forVariable(variable), INITS);
    }

    public QStoreLocation(Path<? extends StoreLocation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QStoreLocation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QStoreLocation(PathMetadata metadata, PathInits inits) {
        this(StoreLocation.class, metadata, inits);
    }

    public QStoreLocation(Class<? extends StoreLocation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.carpart = inits.isInitialized("carpart") ? new QCarpart(forProperty("carpart")) : null;
    }

}

