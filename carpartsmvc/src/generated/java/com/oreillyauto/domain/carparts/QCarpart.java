package com.oreillyauto.domain.carparts;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCarpart is a Querydsl query type for Carpart
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCarpart extends EntityPathBase<Carpart> {

    private static final long serialVersionUID = -1371872043L;

    public static final QCarpart carpart = new QCarpart("carpart");

    public final ListPath<CarpartDetail, QCarpartDetail> carpartDetailsList = this.<CarpartDetail, QCarpartDetail>createList("carpartDetailsList", CarpartDetail.class, QCarpartDetail.class, PathInits.DIRECT2);

    public final StringPath description = createString("description");

    public final StringPath imageName = createString("imageName");

    public final StringPath line = createString("line");

    public final StringPath partNumber = createString("partNumber");

    public final ListPath<StoreLocation, QStoreLocation> storeLocations = this.<StoreLocation, QStoreLocation>createList("storeLocations", StoreLocation.class, QStoreLocation.class, PathInits.DIRECT2);

    public final StringPath title = createString("title");

    public QCarpart(String variable) {
        super(Carpart.class, forVariable(variable));
    }

    public QCarpart(Path<? extends Carpart> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCarpart(PathMetadata metadata) {
        super(Carpart.class, metadata);
    }

}

