package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	CarpartsRepository carpartsRepo;
	
	@Override
	public void saveCarpart(Carpart carpart) {
		carpartsRepo.save(carpart);
	}
	
    @Override
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {    	
        // Old way (fake it 'til you make it)
    	//return carpartsRepo.getCarpartByPartNumber(partNumber);
    	
    	// Querydsl
    	return carpartsRepo.getCarpart(partNumber);
    	
    	// CRUD REPO API
    	//return carpartsRepo.findById(partNumber);
    	
    	// SPRING DATA API
    	//return carpartsRepo.findByPartNumber(partNumber);
    }

	@Override
	public List<Carpart> getCarparts() {
		//return carpartsRepo.getCarparts();
		
		// CRUD REPO API
		return (List<Carpart>)carpartsRepo.findAll();
	}

	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    carpartsRepo.delete(carpart);
	}

}
