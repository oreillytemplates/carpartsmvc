package com.oreillyauto.controllers;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsController {

	@Autowired
	CarpartsService carpartsService;

    @GetMapping(value = { "/carparts/partnumber" })
    public String getPartnumber(Model model, String partnumber) throws Exception {
    	
        if (partnumber != null && partnumber.length() > 0) {
            Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
            model.addAttribute("carpart", carpart);
        }

    	model.addAttribute("active", "add");
        return "partnumber";
    }
    
	@PostMapping(value = { "/carparts/partnumber" })
	public String postPartnumber(Model model, String partnumber, String title, String update) {
	    System.out.println("partnumber=>" + partnumber + " title=>" + title);
        Carpart carpart = new Carpart();
        carpart.setPartNumber(partnumber);
        carpart.setTitle(title);
        carpart.setLine("");
        carpart.setDescription("");        
        carpartsService.saveCarpart(carpart);
        model.addAttribute("messageType", "success");
        
//        model.addAttribute("message", "Part Number " + partnumber+ " Saved Successfully!");
        
        String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";
        model.addAttribute("message", "Part Number " + partnumber + " "+action+" Successfully!");
        
	    return "partnumber";
	}

	@GetMapping(value = { "/carparts/delete/{partnumber}" })
	public String deleteCarpart(@PathVariable String partnumber, Model model) throws Exception {
	        
	    if (partnumber != null && partnumber.length() > 0) {
	        Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
	        carpartsService.deleteCarpartByPartNumber(carpart);
	        model.addAttribute("messageType", "success");
	        model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
	    }
	    
	    return "partnumber";
	}

	@GetMapping(value = { "/carparts/electrical/{partNumber}", "/carparts/engine/{partNumber}", "/carparts/other/{partNumber}" })
	public String getCarpart(@PathVariable String partNumber, Model model) throws Exception {
		String error = "";
		Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
		
		if (carpart == null) {
			error = "Sorry, cannot find part number " + partNumber;
		}
			
		model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
		model.addAttribute("active", partNumber);
		model.addAttribute("error", error);
		return "carparts";
	}

	@GetMapping(value = { "/carparts/carpartManager" })
	public String getCarpartManager(Model model) {
		List<Carpart> carpartList = carpartsService.getCarparts();
		model.addAttribute("carpartList", carpartList);
		return "carpartManager";
	}

	@GetMapping(value = { "/carparts" })
	public String carparts(Model model, String firstName, String lastName) throws Exception {
		String fullName = ((firstName != null && firstName.length() > 0) ? firstName : "");
		fullName = ((fullName.length() > 0) ? (firstName + " " + lastName) : "");
		model.addAttribute("fullName", fullName);

		// Convert a List into JSON
		final List<Carpart> carpartList = new ArrayList<Carpart>();
		carpartList.add(new Carpart("p1", "l1", "t1", "d1"));
		carpartList.add(new Carpart("p2", "l2", "t2", "d2"));
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, carpartList);
		final byte[] data = out.toByteArray();
		// System.out.println(new String(data));

		model.addAttribute("carpartList", carpartList);
		model.addAttribute("carpartListJson", new String(data));

		return "carparts";
	}

	@GetMapping(value = { "/carparts/labtwo" })
	public String getLabTwo(Model model) {
		List<Carpart> carpartList = carpartsService.getCarparts();
		model.addAttribute("carpartList", carpartList);
		return "labtwo";
	}

}
