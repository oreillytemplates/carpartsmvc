<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Feedback</h1>
<div class="card">
	<div class="card-body">
	    <div class="row">	
	    	<%-- <form:form method="post" modelAttribute="command" action="${pageContext.request.contextPath}/carparts/feedback"> --%>
	    	<form:form method="post" modelAttribute="command" action="<%=request.getContextPath() %>/carparts/feedback">
	            <div class="form-group col-sm-12">
	                <label for="email">Email</label>
	    			<form:input class="form-control" path="emailAddress" placeholder="Enter Email Address" />
	    			<label for="emailBody" class="mt10">Message</label>
	    			<form:input class="form-control" path="emailBody" placeholder="Your Message" />
	    			<button class="btn btn-primary mt10" type="submit" id="carSubmit">
	        			Submit
	    			</button>
				</div>
	        </form:form>
	    </div>
	</div>
</div>
