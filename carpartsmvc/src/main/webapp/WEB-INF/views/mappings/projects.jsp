<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Bidirectional Mappings</h1>
<h3>Bidirectional One-to-Many</h3>
<hr/>

<h4>Projects</h4>
<c:forEach items="${projectList}" var="project">
	<div>* ${project}</div>
	<c:forEach items="${project.employees}" var="employee">	
		<div>** ${employee}</div>
	</c:forEach>	
</c:forEach>

<hr/>

<h4>Planners</h4>
<c:forEach items="${plannerList}" var="planner">
	<div><strong>* ${planner}</strong></div>
	<c:forEach items="${planner.projects}" var="curProject">	
		<div>** ${curProject}</div>
	</c:forEach>
</c:forEach>

<hr/>

<h4>User Tables</h4>
<c:forEach items="${userTableList}" var="table">
	${table}<br/>
</c:forEach>

<script>	
	orly.ready.then(() => {
	});
</script>
